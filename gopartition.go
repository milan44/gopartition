package gopartition

// Coordinate describes a point in 2D space
type Coordinate struct {
	X    float64
	Y    float64
	Data interface{}
}

// Volume contains and manages Partitions which contain Coordinates
type Volume struct {
	partitions    [][][]Coordinate
	partitionSize float64
	width         float64
	height        float64
	pWidth        int
	pHeight       int
}

// NewVolume creates and initializes a new Volume
func NewVolume(partitionSize float64, width float64, height float64) Volume {
	v := Volume{
		partitionSize: partitionSize,
		width:         width,
		height:        height,
	}
	v.pWidth = int((width / partitionSize) + 1)
	v.pHeight = int((height / partitionSize) + 1)

	partitions := make([][][]Coordinate, v.pHeight)

	for y := 0; y < v.pHeight; y++ {
		partitions[y] = make([][]Coordinate, v.pWidth)
		for x := 0; x < v.pWidth; x++ {
			partitions[y][x] = make([]Coordinate, 0)
		}
	}

	v.partitions = partitions

	return v
}

// Insert inserts a point into the Volume
func (v *Volume) Insert(c Coordinate) {
	x, y := v.Coordinates(c.X, c.Y)

	v.partitions[y][x] = append(v.partitions[y][x], c)
}

// Coordinates returns the partition coordinates for a given point
func (v *Volume) Coordinates(cx float64, cy float64) (int, int) {
	x := int(cx / v.partitionSize)
	y := int(cy / v.partitionSize)

	return x, y
}

// Surrounding returns the surrounding partitions of a given partition (including the partition itself)
func (v Volume) Surrounding(x int, y int) [][]Coordinate {
	locations := make([][]int, 9)
	index := 0
	for i := -1; i < 2; i++ {
		for j := -1; j < 2; j++ {
			locations[index] = []int{
				x + j,
				y + i,
			}
			index++
		}
	}

	partitions := make([][]Coordinate, 9)
	for i := 0; i < 9; i++ {
		lX := locations[i][0]
		lY := locations[i][1]
		if lX >= 0 && lX < v.pWidth && lY >= 0 && lY < v.pHeight {
			partitions[i] = v.partitions[lY][lX]
		} else {
			partitions[i] = []Coordinate{}
		}
	}

	return partitions
}
