package gopartition

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"os/exec"
	"runtime"
	"runtime/pprof"
	"testing"
)

func TestGoPartition(t *testing.T) {
	cpuProfile, err := os.Create("_cpuprofile.prof")
	if err != nil {
		panic("could not create CPU profile: " + err.Error())
	}
	if err := pprof.StartCPUProfile(cpuProfile); err != nil {
		panic("could not start CPU profile: " + err.Error())
	}

	fmt.Print("Running benchmark...")

	for x := 0; x < 10000; x++ {
		volume := NewVolume(10, 100, 100)

		for i := 0; i < 500; i++ {
			p := Coordinate{
				X: float64(rand.Intn(100)),
				Y: float64(rand.Intn(100)),
			}
			volume.Insert(p)
			x, y := volume.Coordinates(p.X, p.Y)
			_ = volume.Surrounding(x, y)
		}
	}

	runtime.GC()

	pprof.StopCPUProfile()
	cpuProfile.Close()

	cpu, _ := exec.Command("go", "tool", "pprof", "-png", "_cpuprofile.prof").Output()
	ioutil.WriteFile("cpu-profile.png", cpu, 0777)

	os.Remove("_cpuprofile.prof")

	fmt.Println("Done")
}
