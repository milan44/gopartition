# gopartition

Small library to store and load points in partitions. Useful for collision detection (don't have to iterate over all points, only neighboring partitions).

### Usage

Lets say we have a 2D plane which is 1000 by 1000 pixels in size. The partition size defines how big each partition is going to be.  
For example:

```golang
v := gopartition.NewVolume(100, 1000, 1000)
```

will create a Volume which is split into partitions of 100x100 pixels each. If we now insert a point at for example x=55, y=112. It will end up in the partition x=0, y=1.

```golang
v.Insert(gopartition.Coordinate{
    X: 55,
    Y: 112,
    Data: nil, // Any data that we want to store at that position
})
```

Now if we add a lot of points and we want to check if they overlap, we will iterate over each point, get the partition its in and its surrounding partitions and check if it overlaps with any point in those partitions. This saves a lot of computations, because we don't have to iterate over each point, only over the points in the neighboring partitions.

```golang
for _, point := range myPoints {
    x, y := v.Coordinates(point.X, point.Y)
    partitions := v.Surrounding(x, y)
    // partitions is a 2 dimensional array of Coordinates
}
```
